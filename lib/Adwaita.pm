# SPDX-FileCopyrightText: 2022  Emmanuele Bassi
# SPDX-License-Identifier: LGPL-2.1-or-later

package Adwaita;

=encoding utf8

=head1 NAME

Adwaita - Perl interface to the libadwaita GNOME platform library

=head1 SYNOPSIS

  use Gtk4;
  use Adwaita;

  my $app = Adwaita::Application->new('com.example.App', 'default-flags');
  $app->signal_connect(activate => sub {
      my $win = Gtk4::ApplicationWindow->new($app);
      $win->set_title('Hello, Adwaita');
      $win->present();
  });
  $app->run();

=head1 ABSTRACT

Perl binding to the 1.x series of the libadwaita GNOME platform library.

=head1 DESCRIPTION

The C<Adwaita> module allows a Perl developer to use the libadwaita platform
library for GNOME applications.

=cut

use strict;
use warnings;

use Carp qw/croak/;
use Cairo::GObject;
use Glib::Object::Introspection;
use Exporter;

our @ISA = qw(Exporter);

my $_ADW_BASENAME = 'Adw';
my $_ADW_VERSION = '1';
my $_ADW_PACKAGE = 'Adwaita';

sub import {
    my $class = shift;

    Glib::Object::Introspection->setup(
        basename => $_ADW_BASENAME,
        version => $_ADW_VERSION,
        package => $_ADW_PACKAGE,
    );
}

1;

__END__

=head1 SEE ALSO

=head1 AUTHORS

=head1 COPYRIGHT AND LICENSE

=cut