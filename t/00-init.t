#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;

BEGIN { require Adwaita; }

unless (eval { Adwaita->import; 1 }) {
  my $error = $@;
  if (eval { $error->isa('Glib::Error') &&
             $error->domain eq 'g-irepository-error-quark' })
  {
    BAIL_OUT("OS unsupported: $error");
  } else {
    BAIL_OUT("Cannot load Adwaita: $error");
  }
}

plan tests => 1;

is(Adwaita::get_major_version(), 1, 'check_version fail 1');